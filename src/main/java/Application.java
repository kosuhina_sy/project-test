import java.util.Arrays;
import java.util.Scanner;

public class Application {
    static Scanner scanner = new Scanner(System.in);
    private final MathService mathService = new MathService();

    public static void main(String [] args) {
        final Application application = new Application();
        application.run();
    }

    public int run() {
        final int result = getCommand();
        switch (result) {
            case 1: return sumCalc();
            case 2: return factorialCalc();
            case 3: return fibonacciCalc();
            default: return 0;
        }
    }

    public static int getCommand() {
        int command;
        System.out.print("Введите команду(1 - sum, 2 - factorial, 3 - fibonacci, 4 - выход): ");
        String command_input;
        command_input = scanner.next();
        if (Arrays.asList("1", "2", "3", "4").contains(command_input)) {
            command =  Integer.parseInt (command_input);
        }
        else {
            System.out.println("Ошибка. Попробуйте еще раз.");
            command = getCommand();
        }
        return command;
    }

    public int sumCalc() {
        System.out.println("Введите первый аргумент :");
        String arg1 = scanner.next();
        System.out.println("Введите второй аргумент :");
        String arg2 = scanner.next();
        System.out.println(mathService.sum(arg1, arg2));
        return 0;
    }

    public int fibonacciCalc() {
        System.out.println("Введите аргумент :");
        String arg = scanner.next();
        long f[]= mathService.fibonacci(arg);
        for (int i = 0; i < f.length; i++) {
            System.out.println(f[i]);
        }
        return 0;
    }

    public int factorialCalc() {
        System.out.println("Введите аргумент :");
        String arg = scanner.next();
        System.out.println(mathService.factorial(arg));
        return 0;
    }
//тестовый коммит-1
//тестовый коммит-2
//тестовый коммит-3
//тестовый коммит-3
}
